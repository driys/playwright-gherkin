import { Before, After, setDefaultTimeout, BeforeAll, AfterAll } from "@cucumber/cucumber";
import { Browser, chromium, Page } from "playwright/test";

let page: Page;
let browser: Browser;

const timeout = setDefaultTimeout(40 * 1000);
const edgePath = "C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe" // File Path Microsoft Edge

Before(async () => {
  // browser = await chromium.launch({ headless: false , executablePath: edgePath}); // To run browser using microsoft edge
  browser = await chromium.launch({ headless: false,  args: ['--start-maximized'],});
  const context = await browser.newContext({
    httpCredentials: {
      username: "wgs",
      password: "25K9v47BU52NpegG",
    },
    viewport: null
  });
  page = await context.newPage();
  await page.goto("https://tmdigital.stagingapps.net/login");
  // await page.pause(); //uncomment to debug only
});

After(async () => {
  await browser.close();
});

export { browser, page, timeout};