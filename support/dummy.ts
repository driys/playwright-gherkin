import { test, expect } from '@playwright/test';

// Search Feature

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.locator('input[name="email"]').click();
  await page.locator('input[name="email"]').fill('superadmin@gmail.com');
  await page.locator('input[name="password"]').click();
  await page.locator('input[name="password"]').fill('superadmin');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await page.getByRole('link', { name: ' Project' }).click();
  await page.getByPlaceholder('Search by Project Name').click();
  await page.getByPlaceholder('Search by Project Name').fill('Coba Project Tito 2');
  await page.getByRole('link', { name: 'Coba Project Tito' }).click();
  await page.getByPlaceholder('Search by Feature Name').click();
  await page.getByPlaceholder('Search by Feature Name').fill('Test Feature 4');
  await page.getByRole('gridcell', { name: 'F005' }).click();
});
// Edit Feature

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: 'Test Feature 2' }).click();
  await page.getByRole('link', { name: 'Edit Feature' }).click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).locator('input[name="name"]').click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).locator('input[name="name"]').fill('Test Feature Edited');
  await page.getByRole('button', { name: 'Haris Abdullah ' }).click();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).uncheck();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).check();
  await page.getByRole('button', { name: ' Not Tested Status ' }).click();
  await page.getByRole('link', { name: ' Success' }).click();
  await page.locator('textarea[name="description"]').click();
  await page.locator('textarea[name="description"]').fill('coba recent log yang belum ke soft delete edited');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('heading', { name: ' Test Feature Edited' }).click();
});

// Edit Scenario

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByTitle('Edit').click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').fill('Edited Scenario');
  await page.getByRole('button', { name: 'Haris Abdullah ' }).click();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).uncheck();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).check();
  await page.getByRole('button', { name: 'Low Priority ' }).click();
  await page.getByRole('link', { name: 'Highest' }).click();
  await page.getByRole('button', { name: '   Not Tested Result Status ' }).click();
  await page.getByRole('link', { name: ' Success' }).click();
  await page.locator('#editor-03 div').filter({ hasText: 'coba pre condition coba pre' }).click();
  await page.locator('#editor-03 div').filter({ hasText: 'coba pre condition coba pre' }).fill('Edited Precondition');
  await page.getByText('GivenWhenThen').click();
  await page.getByText('GivenWhenThen').press('ControlOrMeta+a');
  await page.getByText('GivenWhenThen').fill('Edited Steps');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
});

// Delete Scenario

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('row', { name: 'S01  Scenario A  not tested' }).getByRole('list').getByRole('link').click();
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('button', { name: ' Sort By ' }).click();
  await page.getByRole('link', { name: 'Newest' }).click();
});

// Add Scenario

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: 'Test Feature 2' }).click();
  await page.getByRole('heading', { name: ' Test Feature' }).click();
  await page.getByRole('button', { name: ' Add New Scenario' }).click();
  await page.getByRole('heading', { name: 'Add New Scenario' }).click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').fill('Scenario Dummy');
  await page.getByRole('button', { name: '', exact: true }).click();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).check();
  await page.getByRole('button', { name: 'Priority ' }).click();
  await page.getByRole('link', { name: 'Medium' }).click();
  await page.locator('#editor-01 div').first().click();
  await page.locator('#editor-01 div').first().fill('Scenario Dummy Pre-condition');
  await page.locator('#editor-02').getByRole('paragraph').click();
  await page.locator('#editor-02 div').first().fill('Scenario Dummy Steps');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Add', exact: true }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('link', { name: 'Scenario Dummy' }).click();
});

// Add Feature 
test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.locator('input[name="email"]').click();
  await page.locator('input[name="email"]').fill('superadmin@gmail.com');
  await page.locator('input[name="password"]').click();
  await page.locator('input[name="password"]').fill('superadmin');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await page.getByRole('link', { name: ' Project' }).click();
  await page.getByPlaceholder('Search by Project Name').click();
  await page.getByPlaceholder('Search by Project Name').fill('tito');
  await page.getByRole('link', { name: 'Coba Project Tito' }).click();
  await page.getByRole('button', { name: ' Add New Features' }).click();
  await page.getByRole('dialog', { name: 'Delete Project' }).locator('input[name="name"]').click();
  await page.getByRole('dialog', { name: 'Delete Project' }).locator('input[name="name"]').fill('Test Feature 4');
  await page.getByRole('button', { name: '', exact: true }).click();
  await page.getByRole('checkbox', { name: 'Select all' }).check();
  await page.getByRole('dialog', { name: 'Delete Project' }).locator('textarea[name="description"]').click();
  await page.getByRole('dialog', { name: 'Delete Project' }).locator('textarea[name="description"]').fill('coba bikin test feature lagi untuk automate');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('row', { name: 'F005  Test Feature 4 Haris' }).locator('#checkItem').check();
});

// Access and Search Project

test('test', async ({ page }) => {
    await page.getByPlaceholder('Search by Name').click();
    await page.getByPlaceholder('Search by Name').fill('Haris');
    await page.getByPlaceholder('Search by Name').press('Enter');
    await page.getByPlaceholder('Search by Name').click();
    await page.getByPlaceholder('Search by Name').fill('Haris');
    await page.getByRole('link', { name: ' Project' }).click();
    await page.getByRole('heading', { name: 'Projects' }).click();
    await page.getByPlaceholder('Search by Project Name').click();
    await page.getByPlaceholder('Search by Project Name').fill('Dummy');
    await page.goto('https://tmdigital.stagingapps.net/project/detail/99');

});

// Search User

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByPlaceholder('Search by Name').click();
  await page.getByPlaceholder('Search by Name').fill('Dummy');
  await page.getByText('User Dummy').click();
  await page.getByRole('gridcell', { name: 'User Dummy' }).locator('div').click();
});

// Edit User

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('row', { name: 'Dummy User 1 dummymail@' }).getByRole('link').first().click();
  await page.locator('#editName').click();
  await page.locator('#editName').fill('Dummy User 2');
  await page.locator('#editUsername').click();
  await page.locator('#editUsername').fill('dummyuser2');
  await page.locator('#editEmail').click();
  await page.locator('#editEmail').press('ArrowLeft');
  await page.locator('#editEmail').press('ArrowLeft');
  await page.locator('#editEmail').press('ArrowLeft');
  await page.locator('#editEmail').press('ArrowLeft');
  await page.locator('#editEmail').press('ArrowLeft');
  await page.locator('#editEmail').fill('dummymail2@yopmail.com');
  await page.getByRole('button', { name: 'qa ' }).click();
  await page.locator('#editUserForm a').filter({ hasText: 'Admin' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('heading', { name: 'Update User Successfully' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('gridcell', { name: 'dummymail2@yopmail.com' }).click();
});

// Delete User

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('row', { name: 'Automation User 412' }).getByRole('link').nth(1).click();
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
});

// Add New Testing Application

test('test', async ({ page }) => {
  await page.getByRole('link', { name: ' Master Data ' }).click();
  await page.getByRole('link', { name: 'Master Data ' }).click();
  await page.getByRole('link', { name: 'Testing Application' }).click();
  await page.getByRole('heading', { name: 'Testing Application' }).click();
  await page.getByRole('button', { name: ' Add New Data Testing' }).click();
  await page.locator('#newTesting_name').click();
  await page.locator('#newTesting_name').fill('New Testing Application');
  await page.getByRole('button', { name: 'Save Data' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
});

// Delete Testing Application

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('row', { name: 'Test B Jan 4, 2024 super admin' }).getByRole('link').nth(1).click();
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
  await page.getByRole('heading', { name: 'Testing Application successfully deleted.' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
});

// Technology

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: ' Master Data ' }).click();
  await page.getByRole('link', { name: 'Technology' }).click();

  // Sorting Technology
  await page.getByRole('button', { name: ' Sort By ' }).click();
  await page.getByRole('link', { name: 'Newest' }).click();
  await page.getByRole('button', { name: ' Newest ' }).click();
  await page.getByRole('link', { name: 'Oldest' }).click();
  await page.getByPlaceholder('Search by name').click();
  
  // Add New Technology
  await page.getByRole('button', { name: ' Add New Data Technology' }).click();
  await page.locator('#newTech_name').click();
  await page.locator('#newTech_name').fill('React Native 2');
  await page.getByRole('button', { name: 'Save Data' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();

  // Edit Technology
  await page.getByRole('gridcell', { name: 'Technology Dummy' }).click();
  await page.getByRole('row', { name: 'React Native Dec 15, 2023' }).getByRole('link').first().click();
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill('React Native 1');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();

  // Delete Technology
  await page.getByRole('row', { name: 'React Native 1 Dec 15, 2023' }).getByRole('link').nth(1).click();
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('gridcell', { name: 'Flutter' }).click();
});

// Project

// Add New Project

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: ' Project' }).click();
  await page.getByRole('link', { name: ' Add New Project' }).click();
  await page.getByText('Choose File').click();
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill('Project Dummy X');
  await page.locator('textarea[name="description"]').click();
  await page.locator('textarea[name="description"]').fill('Project Dummy X');
  await page.getByRole('button', { name: 'Please Select ' }).click();
  await page.locator('a').filter({ hasText: 'Fixed Bid' }).click();
  await page.getByText('Start Date').click();
  await page.locator('input[name="start_date"]').click();
  await page.getByRole('cell', { name: '31' }).click();
  await page.locator('input[name="end_date"]').click();
  await page.getByRole('cell', { name: '»' }).click();
  await page.getByRole('cell', { name: '31' }).nth(1).click();
  await page.getByRole('button', { name: '', exact: true }).nth(2).click();
  await page.getByLabel('PHP').check();
  await page.getByLabel('Vue JS').check();
  await page.getByLabel('JavaScript').check();
  await page.getByRole('button', { name: 'PHP, Vue JS, JavaScript ' }).click();
  await page.getByRole('button', { name: '', exact: true }).nth(3).click();
  await page.getByLabel('Haris Abdullah Guest').check();
  await page.getByRole('button', { name: '', exact: true }).first().click();
  await page.getByLabel('Haris Abdullah', { exact: true }).check();
  await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
  await page.locator('#form-add div').filter({ hasText: 'Start Date End Date Desktop' }).getByRole('button').click();
  await page.getByLabel('Website', { exact: true }).check();
  await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
  await page.getByRole('button', { name: '', exact: true }).click();
  await page.getByLabel('Jira').check();
  await page.getByLabel('Load Runner').check();
  await page.getByRole('checkbox', { name: 'Gherkin' }).check();
  await page.getByLabel('TestRail').check();
  await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
  await page.getByRole('button', { name: ' Save Project' }).click();
  await page.getByRole('button', { name: 'Add' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
  await page.getByRole('button', { name: ' Sort By ' }).click();
  await page.getByRole('link', { name: 'Newest' }).click();
  await page.getByRole('link', { name: 'Project Dummy X' }).click();
});

// Delete Project

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: 'Project Dummy Automation 559' }).click();
  await page.getByRole('link', { name: ' Delete Project' }).click();
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
});

// Edit Project

test('test', async ({ page }) => {
  await page.goto('https://wgs:25K9v47BU52NpegG@tmdigital.stagingapps.net/login');
  await page.getByRole('link', { name: 'Project Dummy Automation' }).click();
  await page.getByRole('link', { name: 'Edit Project' }).click();
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill('Project Dummy Automation Test');
  await page.getByText('Description of Project Dummy').click();
  await page.getByText('Description of Project Dummy').fill('Description of Project Dummy Automation Test');
  await page.getByRole('button', { name: 'Fixed Bid ' }).click();
  await page.locator('a').filter({ hasText: 'ADMS' }).click();
  await page.locator('#start_date').getByRole('img').click();
  await page.getByRole('cell', { name: '»' }).click();
  await page.getByRole('cell', { name: '15' }).click();
  await page.locator('#end_date').getByRole('img').click();
  await page.getByRole('cell', { name: '»' }).click();
  await page.getByRole('cell', { name: '»' }).dblclick();
  await page.getByRole('cell', { name: '16' }).click();
  await page.getByRole('button', { name: ' Save Project' }).click();
  await page.getByRole('button', { name: 'Save', exact: true }).click();
  await page.getByRole('button', { name: 'Ok' }).click();
});