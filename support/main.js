
const { exec } = require("child_process");
const generateReport = require('./reportGenerator');

const orderFiles = [
  // Login
  // 'features/tmdigital/login/login.feature', // Run Login

  // User Management
  // 'features/tmdigital/userManagement/userManagementPage.feature', // Run User Management Module
  // 'features/tmdigital/userManagement/create_user.feature', // Run Create User
  // 'features/tmdigital/userManagement/edit_user.feature', // Run Edit User
  // 'features/tmdigital/userManagement/delete_user.feature', // Run Delete User

  // Project
  "features/tmdigital/project/add_project.feature", // Run Create Project
  // 'features/tmdigital/project/edit_project.feature', // Run Edit Project
  // 'features/tmdigital/project/delete_project.feature', // Run Delete Project
  // 'features/tmdigital/project/delete_project.feature', // Run Delete Project

  // Feature
  "features/tmdigital/project/feature/add_feature.feature", // Run Create Feature
  "features/tmdigital/project/feature/edit_feature_from_table.feature", // Run Edit Feature from data table
  // "features/tmdigital/project/feature/edit_feature_from_detail.feature", // Run Edit Feature from detail feature
  // "features/tmdigital/project/feature/delete_feature_from_table.feature", // Run Delete Feature from data table
  // "features/tmdigital/project/feature/delete_feature_from_detail.feature", // Run Delete Feature from detail feature
  // "features/tmdigital/project/feature/coba_sorting.feature", //Coba sorting feature
  "features/tmdigital/project/feature/search_feature_list.feature",
  'features/tmdigital/project/delete_project.feature'

  // Scenario
  // "features/tmdigital/project/scenario/add_scenario.feature", // Run Create Scenario
  // "features/tmdigital/project/scenario/edit_scenario_from_table.feature", // Run Edit Scenario from data table
  // "features/tmdigital/project/scenario/edit_scenario_from_detail.feature", // Run Edit Scenario from detail scenario
  // "features/tmdigital/project/scenario/delete_scenario_from_table.feature", // Run Delete Scenario from data table
  // "features/tmdigital/project/scenario/delete_scenario_from_detail.feature", // Run Delete Scenario from detail scenario
  // "features/tmdigital/project/delete_project.feature", // Run Delete Project

  // Master Data - Testing Application
  // 'features/tmdigital/masterData/testingApplication/testing_application_page.feature', // Run Testing Application Page
  // 'features/tmdigital/masterData/testingApplication/add_testing_application.feature', // Run Create Testing Application
  // 'features/tmdigital/masterData/testingApplication/edit_testing_application.feature', // Run Create Testing Application
  // 'features/tmdigital/masterData/testingApplication/delete_testing_application.feature', // Run Delete Testing Application

  // Master Data - Technology
  // 'features/tmdigital/masterData/technology/add_technology.feature', // Run Create Technology
  // 'features/tmdigital/masterData/technology/edit_technology.feature', // Run Edit Technology
  // 'features/tmdigital/masterData/technology/delete_technology.feature' // Run Delete Technology
];

const commandOrderFiles = orderFiles.join().replaceAll(",", " ");

// console.log(commandOrderFiles);

exec(
  `npx cucumber-js ${commandOrderFiles} --require-module ts-node/register --require steps/tmdigital/**/*.ts --format json:reports/main_report.json`,
  (err, stdout, stderr) => {
    if (err) {
      console.error();
      console.error("Error:");
      console.error(err);
      console.error();
    }
    console.log(stdout);
    console.error(stderr);
    //   console.log(commandOrderFiles);
        
     // Generate the report after the test execution completes
    generateReport();
  }
);
