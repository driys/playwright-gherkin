
import { generateRandomNumber, generateRandomString, getRandomValueFromArray, getDifferentRole, getRandomValuesFromArray, getDifferentProjectType, generateLoremIpsum, getDifferentPriority, getDifferentStatus } from './utils';


// Array Role and Randomize Role
const createRoles = ['QA', 'Guest'];
const editRoles = ['QA', 'Guest', 'Admin'];
let selectRole = getRandomValueFromArray(createRoles);
let newRole = getDifferentRole(selectRole, editRoles);


const randomNumber = generateRandomNumber(1, 1000); // Generate random number between 1 to 1000
const randomString = generateRandomString(10); // Generate random string with 10 data size

// Data User
const nameUser = "Automation User "+ randomNumber;
const editNameUser = "Edit User " + randomNumber;
const username = "automationuser" + randomNumber;
const editUsername = "editedautomationuser" + randomNumber;
const emailUser = "automation" + randomNumber + "@yopmail.com";
const updatedEmail = "updatedmail" + randomNumber + "@yopmail.com";


// Data Project
const projectName = "Project Dummy Automation " + randomNumber;
const projectDesc = "Description of " + projectName;

const editProjectName = "Project Dummy Automation " + randomNumber + " Edited";
const editProjectDesc = "Description of " + projectName + " Edited";

const TypeList = ['Fixed Bid', 'ET', 'ADMS'];
let projectType = getRandomValueFromArray(TypeList);
let newType = getDifferentProjectType(projectType, TypeList)


const numToSelect = 3;
const techOpt = ['PHP', 'Golang', 'HTML5', 'Python', 'Vue JS', 'JavaScript', 'React Native', 'NextJS']
const selectTech = getRandomValuesFromArray(techOpt, numToSelect);
const newSelectTech = getRandomValuesFromArray(techOpt, numToSelect);

const testAppOpt = ['Load Runner', 'Trello', 'Testcase Lab', 'Redmine', 'TestRail', 'Gherkin', 'K6', 'Katalon', 'Taiga', 'Jira']
const selectTestApp = getRandomValuesFromArray(testAppOpt, numToSelect);
const newSelectTestApp = getRandomValuesFromArray(testAppOpt, numToSelect);

// Data Feature 
const featureName = "Test Feature " + randomNumber;
const editFeatureName = "Edited Feature Test " + randomNumber;

const featureDetail = "Feature detail from " + featureName;
const editFeatureDetail = "Edit Feature detail from " + editFeatureName;



// Data Scenario
const scenarioName = "Test Scenario " + randomNumber;
const editScenarioName = "Edited Scenario " + randomNumber;

const priorityList = ['Low', 'Medium', 'High', 'Highest'];
const selectPriority = getRandomValueFromArray(priorityList);
const newPriority = getDifferentPriority(selectPriority, priorityList);

const precondition = generateLoremIpsum(20);
const scenarioStep = generateLoremIpsum(30);

const resultList = ['Blocked', 'Failed', 'Success'];
const resultStatus = getRandomValueFromArray(resultList);
const newStatus = getDifferentStatus(resultStatus,resultList);


// Data Testing Application
const testingApplication = "Test App " + randomNumber;
const editTestingApplication = "Edit Test App " + randomNumber;

// Data Technology
const technology = "Technology Dummy " + randomNumber;

export {
    randomNumber, 
    randomString,
    emailUser,
    updatedEmail,
    nameUser,
    editNameUser,
    username,
    editUsername,
    selectRole,
    testingApplication,
    editTestingApplication,
    technology,
    newRole,
    projectName,
    projectDesc,
    selectTech,
    selectTestApp,
    editProjectName,
    editProjectDesc,
    projectType,
    newType,
    newSelectTech,
    newSelectTestApp,
    featureName,
    featureDetail,
    editFeatureName,
    editFeatureDetail,
    scenarioName,
    editScenarioName,
    selectPriority,
    newPriority,
    precondition,
    scenarioStep,
    resultStatus,
    newStatus
}