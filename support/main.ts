import { exec } from 'child_process';

function runCucumberFeature(featureFile: string): Promise<void> {
  return new Promise((resolve, reject) => {
    exec(`npx cucumber-js ${featureFile} --require-module ts-node/register --require steps/tmdigital/**/*.ts`, (error, stdout, stderr) => {
      if (error) {
        console.error(`Error: ${stderr}`);
        reject(error);
      } else {
        console.log(`Output: ${stdout}`);
        resolve();
      }
    });
  });
}

async function runFeaturesInOrder() {
  const featureFiles = [
    'features/tmdigital/userManagement/user_management_view.feature', // Run this second
    'features/tmdigital/userManagement/create_user.feature', // Run this third
    'features/tmdigital/userManagement/edit_user.feature', // Run this forth
    'features/tmdigital/userManagement/delete_user.feature', // Run this fifth
    'features/tmdigital/masterData/testingApplication/testing_application_page.feature', // Run this seven
    'features/tmdigital/masterData/testingApplication/add_testing_application.feature', // Run this eight
  ];

  for (const featureFile of featureFiles) {
    await runCucumberFeature(featureFile);
  }
}

runFeaturesInOrder().catch(error => console.error(`Failed to run tests: ${error}`));
