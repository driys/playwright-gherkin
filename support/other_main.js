// UNUSED

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
function runCucumberFeature(featureFile) {
    return new Promise((resolve, reject) => {
        (0, child_process_1.exec)(`npx cucumber-js ${featureFile} --require-module ts-node/register --require steps/tmdigital/**/*.ts`, (error, stdout, stderr) => {
            if (error) {
                console.error(`Error: ${stderr}`);
                reject(error);
            }
            else {
                console.log(`Output: ${stdout}`);
                resolve();
            }
        });
    });
}
async function runFeaturesInOrder() {
    const featureFiles = [
        // Login
        // 'features/tmdigital/login/login.feature', // Run Login

        // User Management
        // 'features/tmdigital/userManagement/userManagementPage.feature', // Run User Management Module
        // 'features/tmdigital/userManagement/create_user.feature', // Run Create User
        // 'features/tmdigital/userManagement/edit_user.feature', // Run Edit User
        // 'features/tmdigital/userManagement/delete_user.feature', // Run Delete User

        // Project
        'features/tmdigital/project/add_project.feature' // Run Create Project


        // Master Data - Testing Application
        // 'features/tmdigital/masterData/testingApplication/testing_application_page.feature', // Run Testing Application Page
        // 'features/tmdigital/masterData/testingApplication/add_testing_application.feature', // Run Create Testing Application
        // 'features/tmdigital/masterData/testingApplication/edit_testing_application.feature', // Run Create Testing Application
        // 'features/tmdigital/masterData/testingApplication/delete_testing_application.feature', // Run Delete Testing Application --> Masih Error

        // Master Data - Technology
        // 'features/tmdigital/masterData/technology/add_technology.feature', // Run Create Technology
        // 'features/tmdigital/masterData/technology/edit_technology.feature', // Run Edit Technology
        // 'features/tmdigital/masterData/technology/delete_technology.feature' // Run Delete Technology
    ];
    for (const featureFile of featureFiles) {
        await runCucumberFeature(featureFile);
    }
}
runFeaturesInOrder().catch(error => console.error(`Failed to run tests: ${error}`));

// npx cucumber-js features/tmdigital/login/login.feature --require-module ts-node/register --require steps/tmdigital/login/loginStepDefinitions.ts