// utils.ts

// Function to generate a random string of a given length
export function generateRandomString(length: number): string {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
}

// Function to generate a random number between min and max (inclusive)
export function generateRandomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getRandomValueFromArray<T>(array: T[]): T {
  const randomIndex = Math.floor(Math.random() * array.length);
  return array[randomIndex];
}

export function getRandomValuesFromArray<T>(arr: T[], num: number): T[] {
  const shuffled = arr.sort(() => 0.5 - Math.random());
  return shuffled.slice(0, num);
}

export function getDifferentRole(currentRole: string, roles: string[]): string {
  let newRole = currentRole;
  while (newRole === currentRole) {
    newRole = getRandomValueFromArray(roles);
  }
  return newRole;
}

export function getDifferentProjectType(currentType: string, projType: string[]): string {
  let newType = currentType;
  while (newType === currentType) {
    newType = getRandomValueFromArray(projType);
  }
  return newType;
}

export function getDifferentPriority(currentPrio: string, priorityList: string[]): string {
  let newPriority = currentPrio;
  while (newPriority === currentPrio) {
    newPriority = getRandomValueFromArray(priorityList);
  }
  return newPriority;
}

export function getDifferentStatus(currentStatus: string, resultList: string[]): string {
  let newStatus = currentStatus;
  while (newStatus === currentStatus) {
    newStatus = getRandomValueFromArray(resultList);
  }
  return newStatus;
}

export function generateLoremIpsum(wordCount: number): string {
  const loremIpsumWords: string[] = [
    "Lorem ", "ipsum ", "dolor ", "sit ", "amet ", "consectetur ", "adipiscing ", "elit ", 
    "sed ", "do ", "eiusmod ", "tempor ", "incididunt ", "ut ", "labore ", "et ", "dolore ", 
    "magna ", "aliqua ", "Ut ", "enim ", "ad ", "minim ", "veniam ", "quis ", "nostrud ", 
    "exercitation ", "ullamco ", "laboris ", "nisi ", "ut ", "aliquip ", "ex ", "ea ", 
    "commodo ", "consequat ", "Duis ", "aute ", "irure ", "dolor ", "in ", "reprehenderit ", 
    "in ", "voluptate ", "velit ", "esse ", "cillum ", "dolore ", "eu ", "fugiat ", "nulla ", 
    "pariatur ", "Excepteur ", "sint ", "occaecat ", "cupidatat ", "non ", "proident ", 
    "sunt ", "in ", "culpa ", "qui ", "officia ", "deserunt ", "mollit ", "anim ", "id ", "est ", "laborum "
  ]; // 69 Word
  
  let text: string[] = [];
  
  for (let i = 0; i < wordCount; i++) {
    const wordIndex = Math.floor(Math.random() * loremIpsumWords.length);
    text.push(loremIpsumWords[wordIndex]);
  }
  
  return text.join(' ');
}