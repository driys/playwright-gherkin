const reporter = require('cucumber-html-reporter');

function generateReport() {
  const options = {
    theme: 'bootstrap',
    jsonFile: 'reports/main_report.json',
    output: 'reports/main_report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    metadata: {
      "Test Environment": "STAGING",
      "Browser": "Chrome Version 127.0.6533.100 (Official Build) (64-bit)",
      "Platform": "Windows 11",
      "Parallel": "Scenarios",
      "Executed": "Local"
    }
  };
  reporter.generate(options);
  console.log('Report generated successfully!');
}

module.exports = generateReport;