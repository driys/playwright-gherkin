// CustomKeyword.ts
import { Given, Then, When } from "@cucumber/cucumber";
import { page, timeout } from "./hooks";
import { expect } from "playwright/test";
import { editNameUser, nameUser, testingApplication, technology, editTestingApplication, projectName, featureName, scenarioName, editScenarioName, editFeatureName, editProjectName } from "./variables";

timeout;


// <------------------------- LOGIN ------------------------->

// User Login as Superadmin
Given("User already logged in as Superadmin", async () => {
  await page.fill('input[name="email"]', "superadmin@gmail.com");
  await page.fill('input[name="password"]', "superadmin");
  await page.locator('button[class="btn btn-primary btn-user px-5"]').click();
  await page.getByText("Dashboard").first().isVisible();
  // await page.pause();
});

// <------------------------- USER MANAGEMENT ------------------------->

When("User click menu User Management", async () => {
  const xpath = '//*[@id="accordionSidebar"]/li[3]/a/i';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

Then("User should redirected to User Management page", async () => {
  let locator = page.getByRole('heading', { name: 'User Management' });
  await expect(locator).toBeVisible();

});

// User already created

When('User already created', async () => {
  // Locate the specific element within the row with the specified name
  const locator = page.getByRole('row', { name: nameUser }).getByRole('link').nth(1);

  // Assert that the element is visible
  await expect(locator).toBeVisible();
  console.log(nameUser);
});

// User already edited

When('User already edited', async () => {
  // Locate the specific element within the row with the specified name
  const locator = page.getByRole('row', { name: editNameUser }).getByRole('link').nth(1);

  // Assert that the element is visible
  await expect(locator).toBeVisible();
  console.log(editNameUser);
});


// <------------------------- PROJECT ------------------------->

// Access Project Page

When('User click menu Project', async () => {
  const xpath = '//*[@id="accordionSidebar"]/li[4]/a/span';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When('User already at Project page', async () => {
  let locator = page.getByRole('heading', { name: 'Projects' });
  await expect(locator).toBeVisible();
});

When('User click specific Project', async () => {
  // const locator = page.getByRole('link', { name: projectName });
  // locator.click();
  try {
    const addElement = page.getByRole('link', { name: projectName });
    await addElement.click();
  } catch (error) {
    const editElement = page.getByRole('link', { name: editProjectName });
    await editElement.click();
  }
});

When('User already at Project detail page', async () => {
  // let locator = page.getByRole('heading', { name: projectName });
  // await expect(locator).toBeVisible();
  try {
    const locatorAdd = page.getByRole('heading', { name: projectName });
    await expect(locatorAdd).toBeVisible();
    console.log(`${projectName} is visible.`);
  } catch (error) {
    try {
      const locatorEdit = page.getByRole('heading', { name: editProjectName });
      await expect(locatorEdit).toBeVisible();
      console.log(`${editProjectName} is visible.`);
    } catch (error) {
      console.error(`Neither ${projectName} nor ${editProjectName} is visible.`);
    }
  }
})

// Sorting Project - Newest

When("User sorting data project to Newest", async () => {
  const xpathCard = '//*[@id="projectCardList"]/div[1]';
  const xpathSort = '//*[@id="text-sortby"]';
  await page.waitForSelector(`xpath=${xpathCard}`);

  // Click the element using the XPath locator
  const elementCard = page.locator(xpathCard);
  const elementSort = page.locator(xpathSort);
  if (elementCard) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Newest' }).click();
  } else {
    console.log('Element not found');
  }
})

// Sorting Project - Oldest

When("User sorting data project to Oldest", async () => {
  const xpathCard = '//*[@id="projectCardList"]/div[1]';
  const xpathSort = '//*[@id="text-sortby"]';
  await page.waitForSelector(`xpath=${xpathCard}`);

  // Click the element using the XPath locator
  const elementCard = page.locator(xpathCard);
  const elementSort = page.locator(xpathSort);
  if (elementCard) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Oldest' }).click();
  } else {
    console.log('Element not found');
  }
})

// Sorting Project - Project Name A-Z

When("User sorting data project to Project Name A-Z", async () => {
  const xpathCard = '//*[@id="projectCardList"]/div[1]';
  const xpathSort = '//*[@id="text-sortby"]';
  await page.waitForSelector(`xpath=${xpathCard}`);

  // Click the element using the XPath locator
  const elementCard = page.locator(xpathCard);
  const elementSort = page.locator(xpathSort);
  if (elementCard) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Project Name A-Z' }).click();
  } else {
    console.log('Element not found');
  }
})

// Sorting Project - Project Name Z-A

When("User sorting data project to Project Name Z-A", async () => {
  const xpathCard = '//*[@id="projectCardList"]/div[1]';
  const xpathSort = '//*[@id="text-sortby"]';
  await page.waitForSelector(`xpath=${xpathCard}`);

  // Click the element using the XPath locator
  const elementCard = page.locator(xpathCard);
  const elementSort = page.locator(xpathSort);
  if (elementCard) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Project Name Z-A' }).click();
  } else {
    console.log('Element not found');
  }
})

// Filter Project - List View

When("User filter data project to List View", async () => {
  const xpath = '//*[@id="content"]/div[1]/div[2]/div[2]/ul/li[1]/a';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    element.click();
  } else {
    console.log('Element not found');
  }
});

// Filter Project - Card View

When("User filter data project to Card View", async () => {
  const xpath = '//*[@id="grid-tab"]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    element.click();
  } else {
    console.log('Element not found');
  }
});

// Project - Save Action

When(`User click the "Save Project" button`, async () => {
  await page.getByRole('button', { name: 'Save Project' }).click();
});

When(`User click the "Save" button on project page`, async () => {
  const xpath = '//*[@id="savedProjectSuccess"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Ok" button on project page confirmation popup`, async () => {
  await page.getByRole('button', { name: 'Ok' }).click();
});

// <------------------------- FEATURE ------------------------->

// Sorting Feature - Newest

When("User sorting data feature to Newest", async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: 'Newest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Feature - Oldest

When("User sorting data feature to Oldest", async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await page.getByRole('button', { name: ' Sort By ' }).click();
    await page.getByRole('link', { name: 'Oldest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Feature Name A-Z

When("User sorting data table feature to Feature Name A-Z", async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]'; //Add child to xpath to clarify locator
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: 'Feature Name A-Z' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Feature Name Z-A

When("User sorting data table feature to Feature Name Z-A", async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]'; //Add child to xpath to clarify locator
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: 'Feature Name Z-A' }).click();
  } else {
    console.log('Element not found');
  }
});

// Feature Detail

When('User click specific Feature', async () => {
  try {
    const addElement = page.getByRole('link', { name: featureName });
    await addElement.click();
  } catch (error) {
    const editElement = page.getByRole('link', { name: editFeatureName });
    await editElement.click();
  }
});

When('User already at Feature detail page', async () => {
  try {
    const locatorAdd = page.getByRole('heading', { name: featureName });
    await expect(locatorAdd).toBeVisible();
  } catch (error) {
    const locatorEdit = page.getByRole('heading', { name: editFeatureName });
    await expect(locatorEdit).toBeVisible();
  }
});

// Add & Edit Feature Save Action

When(`User click the "Save" button on feature modal`, async () => {
  await page.getByRole('button', { name: 'Save' }).click();
});

When(`User click the "Save" button on feature confirmation popup`, async () => {
  const xpath = '//*[@id="btnAddFeature"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Save" button on edit feature confirmation popup`, async () => {
  const xpath = '//*[@id="btnSaveEdit"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Ok" button on feature confirmation popup`, async () => {
  await page.getByRole('button', { name: 'Ok' }).click();
});

// <------------------------- SCENARIO ------------------------->

// Sorting Scenario - Newest

When('User sorting data scenario to Newest', async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: 'Newest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Scenario - Oldest

When('User sorting data scenario to Oldest', async () => {
  const xpath = '(//a[@id="sortDropdown"]//span)[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: 'Oldest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Add & Edit Scenario Save Action

When(`User click the "Save" button on scenario modal`, async () => {
  await page.getByRole('button', { name: 'Save' }).click();
});

When(`User click the "Add" button on scenario confirmation popup`, async () => {
  await page.getByRole('button', { name: 'Add', exact: true }).click();
});

When(`User click the "Save" button on scenario confirmation popup`, async () => {
  await page.locator('//button[@id="updatedSuccess"]').click();
});

When(`User click the "Ok" button on scenario confirmation popup`, async () => {
  try {
    await page.getByRole('button', { name: 'Ok' }).click();
  } catch (error) {
    await page.locator('//*[@id="updatedSuccessModal"]/div/div/div[2]/div/button').click();
  }
});

// Scenario Detail

When('User click specific Scenario', async () => {
  try {
    const addElement = page.getByRole('link', { name: scenarioName });
    await addElement.click();
  } catch (error) {
    const editElement = page.getByRole('link', { name: editScenarioName });
    await editElement.click();
  }
});

When('User already at Scenario detail page', async () => {
  try {
    const locatorAdd = page.getByRole('heading', { name: scenarioName });
    await expect(locatorAdd).toBeVisible();
  } catch (error) {
    const locatorEdit = page.getByRole('heading', { name: editScenarioName });
    await expect(locatorEdit).toBeVisible();
  }
});


// <------------------------- MASTER DATA ------------------------->

// Search Data

When(`User input "Name" at search user by name`, async () => {
  let search = page.getByPlaceholder('Search by name');
  await search.click();
  await search.fill('Dummy');
});

// Search Master Data - Technology by Name

When('User search User by Name', async () => {
  await page.getByPlaceholder('Search by name').click();
  await page.getByPlaceholder('Search by name').fill(technology);
});

// Show Data search Master Data Technology by Name

Then('Data User should showing on table', async () => {
  const locator = page.getByText(technology);
  await expect(locator).toBeVisible();
});

// TESTING APPLICATION

// Access Testing Application page

Given("User already at Testing Application page", async () => {
  let locator = page.getByRole('heading', { name: 'Testing Application' });
  await expect(locator).toBeVisible();
});

When("User click menu Master Data", async () => {
  const xpath = '//*[@id="accordionSidebar"]/li[5]/a/span';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When("User click submenu Testing Application", async () => {
  const xpath = '//*[@id="collapseTwo"]/div/a[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

Then("User should redirected to Testing Application page", async () => {
  let locator = page.getByRole('heading', { name: 'Testing Application' });
  await expect(locator).toBeVisible();
});


// Assert Testing Application already created
When("Testing Application already created", async () => {
  const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  const nameLocator = page.getByText(testingApplication);
  if (element) {
    await nameLocator.click();
    console.log(`App ${testingApplication} is found. \n`);
  } else {
    console.log(`App ${testingApplication} is not found. \n`);
  }
})

// Assert Testing Application already edited
When("Testing Application already edited", async () => {
  const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  const nameLocator = page.getByText(editTestingApplication);
  if (element) {
    await nameLocator.click();
    console.log(`App ${editTestingApplication} is found. \n`);
  } else {
    console.log(`App ${editTestingApplication} is not found. \n`);
  }
})

// Sorting Master Data - Testing Application to Newest

When("User sorting data table Testing Application to Newest", async () => {
  const xpathImg = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
  const xpathSort = '//*[@id="sortDropdown"]';
  await page.waitForSelector(`xpath=${xpathImg}`);

  // Click the element using the XPath locator
  const elementImg = page.locator(xpathImg);
  const elementSort = page.locator(xpathSort);
  if (elementImg) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Newest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Master Data - Testing Application to Oldest

When("User sorting data table Testing Application to Oldest", async () => {
  const xpathImg = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
  const xpathSort = '//*[@id="sortDropdown"]';
  await page.waitForSelector(`xpath=${xpathImg}`);

  // Click the element using the XPath locator
  const elementImg = page.locator(xpathImg);
  const elementSort = page.locator(xpathSort);
  if (elementImg) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Oldest' }).click();
  } else {
    console.log('Element not found');
  }
})

// TECHNOLOGY

// Master Data - Technology

When("User click submenu Technology", async () => {
  const xpath = '//*[@id="collapseTwo"]/div/a[1]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

Given("User already at Technology page", async () => {
  let locator = page.getByRole('heading', { name: 'Technology' });
  await expect(locator).toBeVisible();
});

// Assert Technology already created
When("Technology already created", async () => {
  const nameLocator = page.getByText(technology);
  if (await nameLocator.isVisible()) {
    console.log(`App ${technology} is created. \n`);
    await nameLocator.click();
  } else {
    console.log(`App ${technology} is not created. \n`);
  }
})

// Sorting Master Data - Technology to Newest

When("User sorting data table Technology to Newest", async () => {
  const xpathImg = '//*[@id="technologyDataTable"]/tbody/tr[1]/td[5]/ul/li[2]/a/img';
  const xpathSort = '//*[@id="sortDropdown"]';
  await page.waitForSelector(`xpath=${xpathImg}`);

  // Click the element using the XPath locator
  const elementImg = page.locator(xpathImg);
  const elementSort = page.locator(xpathSort);
  if (elementImg) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Newest' }).click();
  } else {
    console.log('Element not found');
  }
});

// Sorting Master Data - Technology to Oldest

When("User sorting data table Technology to Oldest", async () => {
  const xpathImg = '//*[@id="technologyDataTable"]/tbody/tr[1]/td[5]/ul/li[2]/a/img';
  const xpathSort = '//*[@id="sortDropdown"]';
  await page.waitForSelector(`xpath=${xpathImg}`);

  // Click the element using the XPath locator
  const elementImg = page.locator(xpathImg);
  const elementSort = page.locator(xpathSort);
  if (elementImg) {
    await elementSort.click();
    await page.getByRole('link', { name: 'Oldest' }).click();
  } else {
    console.log('Element not found');
  }
});
