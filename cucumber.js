// module.exports = {
//   default: "--require steps/**/*.ts",
// };

const common = [
  // "features/tmdigital/**/*.feature", // Specify our feature files
  // "--require-module ts-node/register", // Load TypeScript module
  // "--require steps/tmdigital/**/*.ts", // Load step definitions
  // "--require steps/tmdigital/**/**/*.ts", // Load step definitions
  "--require support/hooks.ts", // Load Hooks before and after
  "--require support/otherKeywords.ts", // Load CustomKeyword
  "--require support/utils.ts", // Load utils
  "--require support/dataset.ts" // Load dataset
  // "--format progress", // Load custom formatter
  // "--publish-quiet", // Suppress publishing information
].join(" ");

module.exports = {
  default: common,
};
