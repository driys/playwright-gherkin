# Use an official Node.js runtime as the base image
FROM node

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Install Playwright and browsers
RUN npx playwright install

# Set the playwright environment variable to run in headless mode
ENV PLAYWRIGHT_BROWSERS_PATH=/usr/lib/node_modules/playwright/.local-browsers

# Expose port 3000 (if needed)
# EXPOSE 3000

# Copy the rest of your application code
COPY . .

# Define the command to run your tests
CMD ["npm", "run", "test:cucumber"]