@MasterData
Feature: Technology

    Background: User Login and access Technology page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Technology
        And User sorting data table Technology to Newest
        # And Technology already created

    @valid
    Scenario: Delete Technology
        Given User already at Technology page
        When User click the "Delete" button at spesific Technology
        And User click the "Yes, Delete" button on confirmation popup delete Technology
        And User click the "Ok" button on popup infomartion message delete Technology
        Then Delete Technology should be successfully
        And Technology should be deleted from data table

