@MasterData
Feature: Technology

    Background: User Login and access Technology page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Technology
        And User sorting data table Technology to Newest
    # And Technology already created

    @valid
    Scenario: Edit Technology
        Given User already at Technology page
        When User click the "Edit" button at spesific Technology
        And User edit Technology Name
        And User click the "Save" button on Edit Technology
        And User click the "Save" button on popup confirmation Edit Technology
        And User click the "Ok" button on Edit Technology confirmation popup
        Then Edit Technology should be successfully

