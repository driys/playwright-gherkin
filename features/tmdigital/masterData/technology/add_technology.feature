@MasterData
Feature: Technology

    Background: User Login and access Technology page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Technology

    @valid
    Scenario: Add New Technology
        Given User already at Technology page
        When User click the "Add New Data" button on Technology page
        And User input Technology Name
        And User click the "Save Data" button on add Technology popup
        And User click the "Save" button on Add New Technology
        And User click the "Ok" button on Add New Technology confirmation popup
        And User sorting data table Technology to Newest
        Then Add New Technology success and data shown in data table

