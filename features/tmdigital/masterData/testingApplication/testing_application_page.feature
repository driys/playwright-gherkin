@MasterData
Feature: Testing Application

    Background:
        Given User already logged in as Superadmin

    Scenario: Access Testing Application page
        When User click menu Master Data
        And User click submenu Testing Application
        Then User should redirected to Testing Application page
