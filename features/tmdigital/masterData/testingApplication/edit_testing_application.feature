@MasterData
Feature: Testing Application

    Background: User Login and access Testing Application page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Testing Application
        And User sorting data table Testing Application to Newest
        And Testing Application already created

    @valid
    Scenario: Edit Testing Application
        Given User already at Testing Application page
        When User click the "Edit" button at spesific Testing Application
        And User edit Data Name
        And User click the "Save Data" button on Edit Testing Application
        And User click the "Save" button on Edit Testing Application
        And User click the "Ok" button on Edit Testing Application confirmation popup
        Then Edit Testing Application success and data shown in data table

