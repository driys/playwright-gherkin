@MasterData
Feature: Testing Application

    Background: User Login and access Testing Application page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Testing Application

    @valid
    Scenario: Add New Testing Application
        Given User already at Testing Application page
        When User click the "Add New Data" button on Testing Application page
        And User input Testing Application Name
        And User click the "Save Data" button on Add New Testing Application popup
        And User click the "Save" button on Add New Testing Application
        And User click the "Ok" button on Add New Testing Application confirmation popup
        And User sorting data table Testing Application to Newest
        Then Add New Testing Application success and data shown in data table

