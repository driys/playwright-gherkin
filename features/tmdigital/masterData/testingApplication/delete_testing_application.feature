@MasterData
Feature: Testing Application

    Background: User Login and access Testing Application page
        Given User already logged in as Superadmin
        And User click menu Master Data
        And User click submenu Testing Application
        And User sorting data table Testing Application to Newest
        And Testing Application already edited

    @valid
    Scenario: Delete Testing Application
        Given User already at Testing Application page
        When User click the "Delete" button at spesific Testing Application
        And User click the "Yes, Delete" button on confirmation popup delete Testing Application
        And User click the "Ok" button on popup infomartion message delete Testing Application
        Then Delete Testing Application should be successfully
        And Testing Application should be deleted from data table

