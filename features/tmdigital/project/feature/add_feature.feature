@feature
Feature: Feature

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project

    @valid
    Scenario: Add Feature
        Given User already at Project page
        And User sorting data project to Newest
        When User click specific Project
        And User click button "Add New Feature" 
        And User input Feature Name
        And User select QA Assigned to
        And User input Feature Detail
        And User click the "Save" button on feature modal
        And User click the "Save" button on feature confirmation popup
        And User click the "Ok" button on feature confirmation popup
        Then Add New Feature success and data shown in data table