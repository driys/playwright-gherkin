@Feature
Feature: Feature

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project

    @detail
    Scenario: Delete Feature from scenario detail
        Given User already at Project detail page
        When User click specific Feature
        When User click the "Delete Feature" button on feature detail page
        And User click the "Yes, Delete" button on feature modal
        And User click the "Ok" button on feature confirmation popup
        Then Feature should be deleted from data table