@feature
Feature: Feature

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project

    @detail
    Scenario: Edit Feature from feature detail
        Given User already at Project detail page
        When User click specific Feature
        And User click button "Edit Feature" on feature detail page
        And User edit Feature Name
        And User edit QA Assigned to
        And User edit Status Feature
        And User edit Feature Detail
        And User click the "Save" button on feature modal
        And User click the "Save" button on edit feature confirmation popup
        And User click the "Ok" button on feature confirmation popup
        Then Edit feature success and feature name changes to new name