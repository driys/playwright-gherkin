@Project
Feature: Project

Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        # And Project already created

    @valid
    Scenario: Edit Project
        Given User already at Project page
        When User click specific Project
        And User click "Edit Project" button
        And User edit Project Logo
        And User edit Project Name
        And User edit Project Description
        And User edit Project Type
        And User edit Project Start Date
        And User edit Project End Date
        And User edit used Technology
        And User edit Guest Assigned
        And User edit QA Assigned
        And User edit used Platform Type
        And User edit used Testing Application
        And User click the "Save Project" button
        And User click the "Save" button on project page
        And User click the "Ok" button on project page confirmation popup
        And User sorting data project to Newest
        Then Edit Project success and data shown in data table

