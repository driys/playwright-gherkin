@Scenario
Feature: Scenario

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project
        And User click specific Feature
        And User click specific Scenario

    @valid
    Scenario: Edit Scenario from detail scenario page
        Given User already at Scenario detail page
        When User click the "Edit Scenario" button on scenario detail page
        And User edit Scenario Name from detail scenario
        And User edit Assigned to
        And User edit Priority
        And User edit Result Status
        And User edit Pre-condition
        And User edit Scenario Steps
        And User click the "Save" button on scenario modal
        And User click the "Save" button on scenario confirmation popup
        And User click the "Ok" button on scenario confirmation popup
        And User sorting data scenario to Newest
        Then Edit scenario success and scenario name changes to new name

