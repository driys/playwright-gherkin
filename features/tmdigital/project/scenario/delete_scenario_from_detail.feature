@Scenario
Feature: Scenario

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project
        And User click specific Feature

    @detail
    Scenario: Delete Scenario from scenario detail
        Given User already at Feature detail page
        When User click specific Scenario
        And User click the "Delete Scenario" button on detail scenario page
        And User click the "Yes, Delete" button on scenario modal
        And User click the "Ok" button on scenario confirmation popup
        Then Scenario should be deleted from data table

