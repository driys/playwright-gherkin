@Scenario
Feature: Scenario

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project
        And User click specific Feature

    @valid
    Scenario: Add New Scenario
        Given User already at Feature detail page
        When User click the "Add New Scenario" button on Scenario page
        And User input Scenario Name
        And User select Assigned to
        And User select Priority
        And User input Pre-condition
        And User input Scenario Steps
        And User click the "Save" button on scenario modal
        And User click the "Add" button on scenario confirmation popup
        And User click the "Ok" button on scenario confirmation popup
        And User sorting data scenario to Newest
        Then Add New scenario success and data shown in data table

