@Scenario
Feature: Scenario

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        And User click specific Project
        And User click specific Feature

    @valid
    Scenario: Edit Scenario from data table
        Given User already at Feature detail page
        When User click the "Edit Scenario" button on scenario table list
        And User edit Scenario Name from data table
        And User edit Assigned to
        And User edit Priority
        And User edit Result Status   
        And User edit Pre-condition
        And User edit Scenario Steps
        And User click the "Save" button on scenario modal
        And User click the "Save" button on scenario confirmation popup
        And User sorting data scenario to Newest
        Then Edit scenario success and edited data shown in data table

