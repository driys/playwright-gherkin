@Project
Feature: Project

Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project
        And User sorting data project to Newest
        # And Project already created

    @valid
    Scenario: Delete Project
        Given User already at Project page
        When User click specific Project
        And User click the "Delete Project" button
        And User click the "Yes, Delete" button on confirmation popup delete Project
        And User click the "Ok" button on popup infomartion message delete Project
        And User sorting data project to Newest
        Then Delete Project should be successfully
        And Project should be deleted from data table

