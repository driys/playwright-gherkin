@Project
Feature: Project

    Background: User Login and access Project page
        Given User already logged in as Superadmin
        And User click menu Project

    @valid
    Scenario: Add New Project
        Given User already at Project page
        When User click the "Add New Project" button on Project page
        And User input Project Logo
        And User input Project Name
        And User input Project Description
        And User select Project Type
        And User select Project Start Date
        And User select Project End Date
        And User select used Technology
        And User select Guest Assigned
        And User select QA Assigned
        And User select used Platform Type
        And User select used Testing Application
        And User click the "Save Project" button
        And User click the "Save" button on project page
        And User click the "Ok" button on project page confirmation popup
        And User sorting data project to Newest
        Then Add New Project success and data shown in data table

