@UserManagement
Feature: User Management Page


    @view
    Scenario: Access User Management page
        Given User already logged in as Superadmin
        When User click menu User Management
        Then User should redirected to User Management page


    @create
    Scenario: Create new User using valid data
        Given User already at User Management page
        When User click the "Add New User" button
        And User input name
        And User input username
        And User input email
        And User select role
        And User click the "Add User" button
        And User click the "Add" button on popup confimartion message
        And User click the "Yes" button on popup message
        Then System should show popup message craete new user success
        And New created user is shown in data table

    @edit
    Scenario: Edit User using valid data
        Given User already at User Management page
        When User click the "Edit User" button at spesific user
        And User edit name
        And User edit username
        And User edit email
        And User edit selected role
        And User click the "Save" button on edit user modal
        And User click the "Save" button on confirmation popup
        Then System should show popup message edit user success
        And Edited user should shown in data table

    @delete
    Scenario: Delete User
        Given User already at User Management page
        When User click the "Delete User" button at spesific user
        And User click the "Yes, Delete" button on confirmation popup delete user
        And User click the "Ok" button on popup infomartion message delete user
        Then Delete user should be successfully
        And User should be deleted from data table
