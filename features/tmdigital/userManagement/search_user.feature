@UserManagement
Feature: Search User

   @searchValid
    Scenario: Search User dengan data invalid
        Given User already logged in as Superadmin
        When User click menu User Management
        And User search User by Name
        Then Show message all matched data user in data table

    @searchInvalid
    Scenario Outline: Search User dengan data invalid
        Given User already logged in as Superadmin
        When User click menu User Management
        And User search User by invalid <name>
        Then Show message "No data available in table" in data table


    Examples:
        | name    |
        | abc     |
        | invalid |
        | 122333  |