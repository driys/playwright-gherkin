@CreateUser
Feature: Create User

    Background: User Login and access User Management page
        Given User already logged in as Superadmin
        And User click menu User Management

    @valid
    Scenario: Create new User using valid data
        Given User already at User Management page
        When User click the "Add New User" button
        And User input name
        And User input username
        And User input email
        And User select role
        And User click the "Add User" button
        And User click the "Add" button on popup confimartion message
        And User click the "Yes" button on popup message
        Then System should show popup message craete new user success
        And New created user is shown in data table
