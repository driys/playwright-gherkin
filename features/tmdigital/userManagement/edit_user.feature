@EditUser
Feature: Edit User

    Background: User Login and access User Management page
        Given User already logged in as Superadmin
        And User click menu User Management
        And User already created  
        
    @edit
    Scenario: Edit User using valid data
        Given User already at User Management page
        When User click the "Edit User" button at spesific user
        And User edit name
        And User edit username
        And User edit email
        And User edit selected role
        And User click the "Save" button on edit user modal
        And User click the "Save" button on confirmation popup
        Then System should show popup message edit user success
        And Edited user should shown in data table
