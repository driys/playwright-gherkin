@DeleteUser
Feature: Delete User

    Background: User Login and access User Management page
        Given User already logged in as Superadmin
        And User click menu User Management
        And User already edited  

    @delete
    Scenario: Delete User
        Given User already at User Management page
        When User click the "Delete User" button at spesific user
        And User click the "Yes, Delete" button on confirmation popup delete user
        And User click the "Ok" button on popup infomartion message delete user
        Then Delete user should be successfully
        And User should be deleted from data table
