@LoginTMDigital
Feature: TM Digital Login

    @valid
    Scenario: Successful Login
        Given I am on the TM Digital login page
        When I enter a valid email address "superadmin@gmail.com" and password "superadmin"
        And I click the "Sign In" button
        Then I should be logged in successfully to TM Digital Dashboard

    @invalid
    Scenario: Invalid Login
        Given I am on the TM Digital login page
        When I enter email address "<email>" and password "<password>"
        And I click the "Sign In" button
        Then Login should be failed
        And User should see error message "<err_msg>"

        Examples:
            | email                | password    | err_msg                    |
            | superadmin@gmail.com | satuduatiga | INVALID CREDENTIAL         |
            | test                 | superadmin  | Email format is not valid! |
            |                      | superadmin  | Please fill out this field |