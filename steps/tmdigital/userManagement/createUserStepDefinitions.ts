// createUserStepDefinitions.ts
import { When, Then, Given } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../support/hooks";
import { nameUser, username, emailUser, selectRole } from "../../../support/variables";

timeout;


Given("User already at User Management page", async () => {
    let locator = page.getByRole('heading', { name: 'User Management' });
    await expect(locator).toBeVisible();
});

When(`User click the "Add New User" button`, async () => {

    // const xpath = '//*[@id="content"]/div/div[1]/button';
    // await page.waitForSelector(`xpath=${xpath}`);
  
    // // Click the element using the XPath locator
    // const element = page.locator(xpath);
    // if (element) {
    //   await element.click();
    // } else {
    //   console.log('Element not found');
    // }
    await page.getByRole('button', { name: ' Add New User' }).click();
 
    // await page.locator('button[class="btn-add btn-add-user btn-primary text-bold text-white"]').click();
});

When("User input name", async () => {
    await page.locator('#newUser_name').click();
    await page.locator('#newUser_name').fill(nameUser);
    console.log(nameUser);
    return nameUser;
});

When("User input username", async () => {
    await page.locator('#newUser_username').click();
    await page.locator('#newUser_username').fill(username);
});

When("User input email", async () => {
    await page.locator('#newUser_email').click();
    await page.locator('#newUser_email').fill(emailUser);
    return emailUser;
});

When("User select role", async () => {
    await page.getByRole('button', { name: 'Please Select ' }).click();
    await page.locator('#createUser a').filter({ hasText: selectRole }).click();
    console.log('\nRole selected is ' + selectRole + '\n')
    return selectedRole;
});

When(`User click the "Add User" button`, async () => {
    await page.getByRole('button', { name: 'Add User' }).click(); 
});

When(`User click the "Add" button on popup confimartion message`, async () => {
    await page.getByRole('button', { name: 'Add', exact: true }).click();
});

When(`User click the "Yes" button on popup message`,async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
})

Then("System should show popup message craete new user success", async () => {
    console.log("\nLet say it work");
});

When("New created user is shown in data table", async () => { //Still failed
    const nameLocator = page.getByText(nameUser);
    if (await nameLocator.isVisible()) {
        console.log(`\nUser ${nameUser} is created`); 
    }else {
        console.log(`\nUser ${nameUser} is not created`);
    }
    
});

const selectedRole = selectRole;
export { nameUser, emailUser, selectedRole }