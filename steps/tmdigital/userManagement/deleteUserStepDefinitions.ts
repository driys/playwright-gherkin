import { When, Then} from "@cucumber/cucumber";
import { page, timeout } from "../../../support/hooks";
import { editNameUser, nameUser } from "../../../support/variables";
import { expect } from "playwright/test";

timeout;

When(`User click the "Delete User" button at spesific user`, async () => {

    // Delete User by parameter created user
    // await page.pause();
    const locator = page.getByRole('row', { name: editNameUser }).getByRole('link').nth(1);
    locator.click();
    // Delete User by first row in data table
    // const xpath = '//*[@id="user-management"]/tbody/tr[1]/td[6]/ul/li[3]/a';
    // const xpathImg = '//*[@id="user-management"]/tbody/tr[1]/td[6]/ul/li[3]/a/img';
    // await page.waitForSelector(`xpath=${xpathImg}`, { state: "visible" });

    // // Click the element using the XPath locator
    // const element = page.locator(`xpath=${xpathImg}`);
    // if (await element.isVisible()) {
    //     element.click();
    //     console.log('Element clicked');
    // } else {
    //     console.log('Element not found or not visible');
    // }
})

When(`User click the "Yes, Delete" button on confirmation popup delete user`, async () => {
    await page.getByRole('button', { name: 'Yes, Delete' }).click();
})

When(`User click the "Ok" button on popup infomartion message delete user`, async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
})

Then("Delete user should be successfully", async () => {
    console.log('User Deleted');
})

When("User should be deleted from data table", async () => {
    const locator = page.getByRole('row', { name: editNameUser }).getByRole('link').nth(1);
    expect(locator).toBeHidden;
})