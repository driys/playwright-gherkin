// searchUserStepDefinitions.ts
import { When, Then, Given } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../support/hooks";
import { nameUser } from "../../../support/variables";

// Search User by Name

When('User search User by Name', async () => {
    await page.getByPlaceholder('Search by Name').click();
    await page.getByPlaceholder('Search by Name').fill(nameUser);
});

// Show Data search User by Name

Then('Show message all matched data user in data table', async () => {
    const locator = page.getByText(nameUser);
    await expect(locator).toBeVisible();
});

// Search User by Invalid Name

When(`User search User by invalid (.*)`, async (name: string) => {
    await page.getByPlaceholder('Search by Name').click();
    await page.getByPlaceholder('Search by Name').fill(name);
});

// Show No data available

Then(`Show message "No data available in table" in data table`, async () => {
    const locator = page.getByText('No data available in table');
    await expect(locator).toBeVisible();
});