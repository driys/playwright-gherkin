import { When, Then, Given } from "@cucumber/cucumber";
import { page, timeout } from "../../../support/hooks";
import { nameUser, selectRole, updatedEmail, newRole, editNameUser, editUsername } from "../../../support/variables";
import { expect } from "playwright/test";

timeout;

When(`User click the "Edit User" button at spesific user`, async () => {
    // Click edit icon by Name User
    await page.getByRole('row', { name: nameUser }).getByRole('link').first().click();

    // Click edit icon by first row in data table

    // await page.pause()
    // const xpathImg = '//*[@id="user-management"]/tbody/tr[1]/td[6]/ul/li[2]/a/img';
    // await page.waitForSelector(`xpath=${xpathImg}`, { state: "visible" });

    // // Click the element using the XPath locator
    // const element = page.locator(`xpath=${xpathImg}`);
    // if (await element.isVisible()) {
    //     element.click();
    //     console.log('Element clicked');
    // } else {
    //     console.log('Element not found or not visible');
    // }
});

When("User edit name", async () => {
    await page.locator('#editName').click();
    await page.locator('#editName').fill(editNameUser);
    return editNameUser;
});

When("User edit username", async () => {
    await page.locator('#editUsername').click();
    await page.locator('#editUsername').fill(editUsername);
    return editUsername;
});

When("User edit email", async () => {
    await page.locator('#editEmail').click();
    await page.locator('#editEmail').fill(updatedEmail);
});

When("User edit selected role", async () => {
    await page.getByRole('button', { name: selectRole }).click();
    await page.locator('#editUserForm a').filter({ hasText: newRole }).click();
    console.log('\n Role changed to '+newRole+'\n')
});

When(`User click the "Save" button on edit user modal`, async () => {
    // const xpath = '//*[@id="btnSaveEdit"]';
    // await page.waitForSelector(`xpath=${xpath}`);
    // const element = page.locator(xpath);
    // if (element) {
    //     element.click();
    // }else{
    //     console.log('Element not found')
    // }
    await page.getByRole('button', { name: 'Save' }).click();
});

When(`User click the "Save" button on confirmation popup`, async () => {
    const xpath = '//*[@id="btnSaveEdit"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    }else{
        console.log('Element not found')
    }
});

Then("System should show popup message edit user success", async () => {
    const xpath = '//*[@id="saveUpdateUser"]/div/div/div[2]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        console.log('\nElement found');
        await page.getByRole('button', { name: 'Ok' }).click();
    } else {
        console.log('\nElement not found');
    }
    // const locator = page.getByRole('heading', { name: 'Update User Successfully' })
    // // Assert popup information edit user shown
    // await expect(locator).toBeVisible();
    // // Click OK button on modal popup
    // await page.getByRole('button', { name: 'Ok' }).click();
});

When("Edited user should shown in data table", async () => {
    const locator = page.getByRole('row', { name: editNameUser }).getByRole('link').nth(1);

    // Assert that the element is visible
    await expect(locator).toBeVisible();
});

export { editNameUser, editUsername }