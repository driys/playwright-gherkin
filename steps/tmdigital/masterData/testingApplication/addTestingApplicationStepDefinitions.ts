// createUserStepDefinitions.ts
import { When, Then, Given } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { testingApplication } from "../../../../support/variables";

timeout;


When(`User click the "Add New Data" button on Testing Application page`, async () => {
    await page.getByRole('button', { name: ' Add New Data Testing' }).click();
});

When("User input Testing Application Name", async () => {
    await page.locator('#newTesting_name').click();
    await page.locator('#newTesting_name').fill(testingApplication);

});

When(`User click the "Save Data" button on Add New Testing Application popup`, async () => {
    await page.getByRole('button', { name: 'Save Data' }).click();
});

When(`User click the "Save" button on Add New Testing Application`, async () => {
    const xpath = '//*[@id="btnAddTesting"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
      await element.click();
    } else {
      console.log('Element not found');
    }
});

When(`User click the "Ok" button on Add New Testing Application confirmation popup`,async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
    // console.log(testingApplication);
    // return testingApplication;
})

Then("Add New Testing Application success and data shown in data table", async () => {
    const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
    await page.waitForSelector(`xpath=${xpath}`);
  
    // Click the element using the XPath locator
    const element = page.locator(xpath);
    const nameLocator = page.getByText(testingApplication);
    if (element) {
      await nameLocator.click();
      console.log(`App ${testingApplication} is successfuly created. \n`);
    } else {
      console.log(`App ${testingApplication} is failed to create. \n`);
    }
});

const testApp = testingApplication;
export { testApp }