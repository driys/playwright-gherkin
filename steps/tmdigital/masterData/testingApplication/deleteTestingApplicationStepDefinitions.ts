// createUserStepDefinitions.ts
import { When, Then } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../../support/hooks";
import { editTestApp } from "./editTestingApplicationStepDefinitions";

timeout;

When(`User click the "Delete" button at spesific Testing Application`, async () => {
      
    // Delete Testing Application by parameter created Testing Application
    await page.getByRole('row', { name: editTestApp }).isVisible();
    await page.getByRole('row', { name: editTestApp }).getByRole('link').nth(1).click();

    // // Delete Testing Application by first row in data table
    // const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[2]/a/img';
    // await page.waitForSelector(`xpath=${xpath}`);

    // // Click the element using the XPath locator
    // const deleteIcon = page.locator(`xpath=${xpath}`);
    // if (await deleteIcon.isVisible()) {
    //     await deleteIcon.click();
    //     console.log('Element clicked');
    // } else {
    //     console.log('Element not found or not visible');
    // }
});

When(`User click the "Yes, Delete" button on confirmation popup delete Testing Application`, async () => {
    await page.getByRole('button', { name: 'Yes, Delete' }).click();
});

When(`User click the "Ok" button on popup infomartion message delete Testing Application`, async () => {
    await page.getByRole('heading', { name: 'Testing Application successfully deleted.' }).click();
    await page.getByRole('button', { name: 'Ok' }).click();
});

Then("Delete Testing Application should be successfully", async () => {
    console.log('Testing Application Deleted');
});

When("Testing Application should be deleted from data table", async () => {
    const locator = page.getByRole('row', { name: editTestApp }).getByRole('link').nth(1);
    expect(locator).toBeHidden;
})