// createUserStepDefinitions.ts
import { When, Then, Given } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../../support/hooks";
import { editTestingApplication, testingApplication } from "../../../../support/variables";
import { testApp } from "./addTestingApplicationStepDefinitions";

timeout;


When(`User click the "Edit" button at spesific Testing Application`, async () => {

    // Edit Testing Application by parameter created Testing Application
    // await page.getByRole('row', { name: testingApplication }).getByRole('link').nth(1).click();
    await page.getByRole('row', { name: testApp }).getByRole('link').first().click();

    // Click Edit Element by first row in data table
    // const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
    // await page.waitForSelector(`xpath=${xpath}`);

    // // Click the element using the XPath locator
    // const deleteIcon = page.locator(`xpath=${xpath}`);
    // if (await deleteIcon.isVisible()) {
    //     await deleteIcon.click();
    //     console.log('Element clicked');
    // } else {
    //     console.log('Element not found or not visible');
    // }
});

When("User edit Data Name", async () => {
    await page.locator('input[name="name"]').click();
    await page.locator('input[name="name"]').fill(editTestingApplication);
});

When(`User click the "Save Data" button on Edit Testing Application`, async () => {
    await page.getByRole('button', { name: 'Save' }).click();
})

When(`User click the "Save" button on Edit Testing Application`, async () => {
    const xpath = '//*[@id="btnSaveEdit"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.click();
    } else {
        console.log('Element not found');
    }
});

When(`User click the "Ok" button on Edit Testing Application confirmation popup`, async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
})

Then("Edit Testing Application success and data shown in data table", async () => {
    const xpath = '//*[@id="testingDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
    await page.waitForSelector(`xpath=${xpath}`);

    const nameLocator = page.getByText(editTestingApplication);
    if (await nameLocator.isVisible()) {
        console.log(`App ${editTestingApplication} is successfuly edited. \n`);
        await nameLocator.click();
    } else {
        console.log(`App ${editTestingApplication} is failed to edit. \n`);
    }
});

const editTestApp = editTestingApplication;
export { editTestApp }