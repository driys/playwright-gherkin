// createUserStepDefinitions.ts
import { When, Then, Given } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { technology } from "../../../../support/variables";

timeout;


When(`User click the "Edit" button at spesific Technology`, async () => {
    // await page.getByRole('row', { name: 'React Native Dec 15, 2023' }).getByRole('link').first().click();

    // Delete Technology by first row in data table
    const xpath = '//*[@id="technologyDataTable"]/tbody/tr[1]/td[5]/ul/li[1]/a/img';
    await page.waitForSelector(`xpath=${xpath}`);

    // Click the element using the XPath locator
    const editIcon = page.locator(`xpath=${xpath}`);
    if (await editIcon.isVisible()) {
        await editIcon.click();
        console.log('Element clicked');
    } else {
        console.log('Element not found or not visible');
    }
});

When("User edit Technology Name", async () => {
    await page.locator('input[name="name"]').click();
    await page.locator('input[name="name"]').fill(technology);

});

When(`User click the "Save" button on Edit Technology`, async () => {
    await page.getByRole('button', { name: 'Save' }).click();
});

When(`User click the "Save" button on popup confirmation Edit Technology`, async () => {
    const xpath = '//*[@id="btnSaveEdit"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.click();
    } else {
        console.log('Element not found');
    }
});

When(`User click the "Ok" button on Edit Technology confirmation popup`, async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
    // console.log(testingApplication);
    // return testingApplication;
})

Then("Edit Technology should be successfully", async () => {
    const nameLocator = page.getByText(technology);
    if (await nameLocator.isVisible()) {
        await nameLocator.waitFor();
        console.log(`App ${technology} is successfuly edited. \n`);
    } else {
        console.log(`App ${technology} is failed to edit. \n`);
    }
});

const tech = technology;
export { tech }