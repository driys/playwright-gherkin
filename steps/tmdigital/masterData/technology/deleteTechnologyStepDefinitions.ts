// deleteTechnology.ts
import { When, Then } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../../support/hooks";
import { technology } from "../../../../support/variables";

timeout;

When(`User click the "Delete" button at spesific Technology`, async () => {
      
    // Delete Technology by parameter created Technology
    // await page.getByRole('row', { name: technology }).getByRole('link').nth(1)

    // Delete Technology by first row in data table
    const xpath = '//*[@id="technologyDataTable"]/tbody/tr[1]/td[5]/ul/li[2]/a/img';
    await page.waitForSelector(`xpath=${xpath}`);

    // Click the element using the XPath locator
    const deleteIcon = page.locator(`xpath=${xpath}`);
    if (await deleteIcon.isVisible()) {
        await deleteIcon.click();
        console.log('Element clicked');
    } else {
        console.log('Element not found or not visible');
    }
});

When(`User click the "Yes, Delete" button on confirmation popup delete Technology`, async () => {
    await page.getByRole('button', { name: 'Yes, Delete' }).click();
});

When(`User click the "Ok" button on popup infomartion message delete Technology`, async () => {
    await page.getByRole('heading', { name: 'Technology successfully deleted.' }).click();
    await page.getByRole('button', { name: 'Ok' }).click();
});

Then("Delete Technology should be successfully", async () => {
    console.log('Technology Deleted');
});

When("Technology should be deleted from data table", async () => {
    const locator = page.getByRole('row', { name: technology }).getByRole('link').nth(1);
    expect(locator).toBeHidden;
})