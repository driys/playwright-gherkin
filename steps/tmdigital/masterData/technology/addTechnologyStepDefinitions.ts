// addTechnologyStepDefinitions.ts
import { When, Then } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { technology } from "../../../../support/variables";

timeout;


When(`User click the "Add New Data" button on Technology page`, async () => {
    await page.getByRole('button', { name: ' Add New Data Technology' }).click();
});

When("User input Technology Name", async () => {
    await page.locator('#newTech_name').click();
    await page.locator('#newTech_name').fill(technology);

});

When(`User click the "Save Data" button on add Technology popup`, async () => {
    await page.getByRole('button', { name: 'Save Data' }).click();
});

When(`User click the "Save" button on Add New Technology`, async () => {
    const xpath = '//*[@id="btnAddTechnology"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
      await element.click();
    } else {
      console.log('Element not found');
    }
});

When(`User click the "Ok" button on Add New Technology confirmation popup`,async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
    // console.log(testingApplication);
    // return testingApplication;
})

Then("Add New Technology success and data shown in data table", async () => {
    const nameLocator = page.getByText(technology);
    if (await nameLocator.isVisible()) {
        await nameLocator.waitFor();
        console.log(`App ${technology} is successfully created. \n`);
    }else {
        console.log(`App ${technology} is failed to create. \n`);
    }
});

const tech = technology;
export { tech }