// loginSteps.ts
import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { expect } from "playwright/test";
import { page, timeout } from "../../../support/hooks";

timeout;

Given("I am on the TM Digital login page", async () => {
  await page.goto("https://tmdigital.stagingapps.net/login");
});

When(
  `I enter a valid email address {string} and password {string}`,
  async (email: string, password: string) => {
    await page.fill('input[name="email"]', email);
    await page.fill('input[name="password"]', password);
  }
);

When(
  "I enter email address {string} and password {string}",
  async function (email, password) {
    await page.fill('input[name="email"]', email);
    await page.fill('input[name="password"]', password);
  }
);

When('I click the "Sign In" button', async () => {
  await page.locator('button[class="btn btn-primary btn-user px-5"]').click();
});

Then("Login should be failed", async () => {
  const locator = page.getByText("Dashboard").first();
  await expect(locator).toBeHidden();
});

Then("User should see error message {string}", async function (err_msg) {
  if (err_msg === "INVALID CREDENTIAL") {
    await page.getByText("INVALID CREDENTIAL ").first().isVisible();
  } else if (err_msg === "Email format is not valid!") {
    await page.getByText("Email format is not valid!").first().isVisible();
  } else {
    console.log("Salah satu field kosong");
  }
  // await browser.close();
});

Then("I should be logged in successfully to TM Digital Dashboard", async () => {
  // Add assertions or verifications here
  // For example, you can assert that a certain element is present on the dashboard page
  await page.getByText("Dashboard").first().isVisible();
  // await browser.close();
});
