import { When, Then, } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { editScenarioName, newPriority, precondition, resultStatus, scenarioName, scenarioStep, selectPriority } from "../../../../support/variables";
import { expect } from "playwright/test";

timeout;

When(`User click the "Edit Scenario" button on scenario table list`, async () => {
  await page.getByTitle('Edit').click();
});

When(`User click the "Edit Scenario" button on scenario detail page`, async () => {
  const xpath = '//*[@id="content"]/div[1]/div[1]/div/a[1]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
})

When('User edit Scenario Name from data table', async () => {
  const locator = page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').first();
  await expect(locator).toBeVisible();
  await locator.click();
  await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').fill(editScenarioName);
});

When('User edit Scenario Name from detail scenario', async () => {
  const xpath = '//*[@id="nameEditScenario"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await element.fill(editScenarioName);
  } else {
    console.log('Element not found');
  }
});

When('User edit Assigned to', async () => {
  const xpath = '//*[@id="form-edit-scenario"]/div[2]/div[1]/div/div[1]/div/button';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
  // await page.getByRole('button', { name: '', exact: true }).click();
  await page.getByRole('checkbox', { name: 'Haris Abdullah' }).uncheck();
  await page.getByRole('checkbox', { name: 'Haris Dummy' }).check();
});

When('User edit Priority', async () => {
  const xpath = '//*[@id="sortDropdownPriority"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: newPriority, exact: true }).click();
  } else {
    console.log('Element not found');
  }
});

When('User edit Result Status', async () => {
  const xpath = '//*[@id="sortDropdownStatus"]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await page.getByRole('link', { name: resultStatus }).click();
  } else {
    console.log('Element not found');
  }
});

When('User edit Pre-condition', async () => {
  const xpath = '//div[@id="editor-03"]//div[1]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await element.fill(precondition);
  } else {
    console.log('Element not found');
  }
});

When('User edit Scenario Steps', async () => {
  const xpath = '//div[@id="editor-04"]//div[1]';
  await page.waitForSelector(`xpath=${xpath}`);
  const element = page.locator(xpath);
  if (element) {
    await element.click();
    await element.fill(scenarioStep);
  } else {
    console.log('Element not found');
  }
});

Then('Edit scenario success and edited data shown in data table', async () => {
  const locator = page.getByRole('link', { name: editScenarioName });
  await expect(locator).toBeVisible();
});

Then('Edit scenario success and scenario name changes to new name', async () => {
  const locator = page.getByRole('heading', { name: editScenarioName });
  await expect(locator).toBeVisible();
})