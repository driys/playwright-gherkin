import { When, Then, } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { editScenarioName, scenarioName } from "../../../../support/variables";
import { expect } from "playwright/test";

timeout;

When(`User click the "Delete Scenario" button on scenario page`, async () => {
  await page.getByRole('row', { name: editScenarioName }).getByRole('list').getByRole('link').click();
});

When(`User click the "Delete Scenario" button on detail scenario page`, async () => {
  const xpath = '//*[@id="content"]/div[1]/div[1]/div/a[2]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Yes, Delete" button on scenario modal`, async () => {
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
});

Then('Scenario should be deleted from data table', async () => {
  const locatorAdd = await page.getByRole('row', { name: scenarioName }).isHidden();
  const locatorEdit = await page.getByRole('row', { name: editScenarioName }).isHidden();

  if (locatorAdd && locatorEdit) {
    console.log(`Scenario is successfully deleted \n`);
  } else {
    console.log(`Failed to delete scenario \n`);
  }
});