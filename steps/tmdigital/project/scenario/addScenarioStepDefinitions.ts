import { When, Then, } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { precondition, scenarioName, scenarioStep, selectPriority } from "../../../../support/variables";
import { expect } from "playwright/test";

timeout;

When(`User click the "Add New Scenario" button on Scenario page`, async () => {
    await page.getByRole('button', { name: ' Add New Scenario' }).click();
    await page.getByRole('heading', { name: 'Add New Scenario' }).click();
});

When('User input Scenario Name', async () => {
    await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').click();
    await page.getByRole('dialog', { name: 'Add New Scenario' }).getByRole('textbox').fill(scenarioName);
});

When('User select Assigned to', async () => {
    const xpath = '//*[@id="form-add"]/div[2]/div[1]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
      await element.click();
    } else {
      console.log('Element not found');
    } 
    // await page.getByRole('button', { name: '', exact: true }).click();
    await page.getByRole('checkbox', { name: 'Haris Abdullah' }).check();
});

When('User select Priority', async () => {
    await page.getByRole('button', { name: 'Priority ' }).click();
    await page.getByRole('link', { name: selectPriority, exact: true}).click();
});

When('User input Pre-condition', async () => {
    await page.locator('#editor-01 div').first().click();
    await page.locator('#editor-01 div').first().fill(precondition);
});

When('User input Scenario Steps', async () => {
    await page.locator('#editor-02').getByRole('paragraph').click();
    await page.locator('#editor-02 div').first().fill(scenarioStep);
});

Then('Add New scenario success and data shown in data table', async () => {
    let locator = page.getByRole('link', { name: scenarioName })
    await expect(locator).toBeVisible();
});