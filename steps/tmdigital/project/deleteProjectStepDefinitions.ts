// deleteProjectStepDefinitions.ts
import { When, Then } from "@cucumber/cucumber";
import { page, timeout } from "../../../support/hooks";
import { projectName } from "../../../support/variables";
import { expect } from "playwright/test";

timeout;


When(`User click the "Delete Project" button`, async () => {
    await page.getByRole('link', { name: ' Delete Project' }).click();
})

When(`User click the "Yes, Delete" button on confirmation popup delete Project`,async () => {
    await page.getByRole('button', { name: 'Yes, Delete' }).click();
})

When(`User click the "Ok" button on popup infomartion message delete Project`, async () => {
    await page.getByRole('button', { name: 'Ok' }).click();
})

Then('Delete Project should be successfully', async () => {
    console.log(projectName + ' successfully deleted');
})

When('Project should be deleted from data table', async () => {
    const locator = page.getByRole('link', { name: projectName });
    expect(locator).toBeHidden;
})

const proj = projectName;
export { proj }