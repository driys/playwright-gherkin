// editProjectStepDefinitions.ts
import { When, Then } from "@cucumber/cucumber";
import { page, timeout } from "../../../support/hooks";
import { selectTech, selectTestApp, editProjectName, editProjectDesc, projectType, newType, newSelectTech, newSelectTestApp } from "../../../support/variables";

timeout;

When(`User click "Edit Project" button`, async () => {
    await page.getByRole('link', { name: 'Edit Project' }).click();
})

When("User edit Project Logo", async () => {
    // await page.getByText('Choose File').click(); 
});

When("User edit Project Name", async () => {
    await page.locator('input[name="name"]').click();
    await page.locator('input[name="name"]').fill(editProjectName);
});

When("User edit Project Description", async () => {
    await page.locator('textarea[name="description"]').click();
    await page.locator('textarea[name="description"]').fill(editProjectDesc);
});

When("User edit Project Type", async () => {
    await page.getByRole('button', { name: projectType }).last().click();
    await page.locator('#form-edit').getByRole('list').locator('a').filter({ hasText: newType }).click();
    console.log('\n Project Type changed to ' + newType + '\n')
});

When("User edit Project Start Date", async () => {
    await page.getByText('Start Date').click();
    await page.locator('input[name="start_date"]').click();

    // Try to select the 31st
    const date31 = page.getByRole('cell', { name: '15' });
    if (await date31.isVisible()) {
        await date31.click();
        console.log('Selected date 15');
    } else {
        // If 31st is not visible, select the 28th
        const date28 = page.getByRole('cell', { name: '20' });
        await date28.waitFor({ state: 'visible' });
        await date28.click();
        console.log('Selected date 20');
    }
});

When("User edit Project End Date", async () => {
    await page.getByText('End Date').click();
    await page.locator('input[name="end_date"]').click();
    await page.getByRole('cell', { name: '»' }).click();
    await page.getByRole('cell', { name: '»' }).click();
    await page.getByRole('cell', { name: '»' }).click();

    // Try to select the 31st
    const date31 = page.getByRole('cell', { name: '15' });
    if (await date31.isVisible()) {
        await date31.click();
        console.log('Selected date 15');
    } else {
        // If 31st is not visible, select the 28th
        const date28 = page.getByRole('cell', { name: '20' });
        await date28.waitFor({ state: 'visible' });
        await date28.click();
        console.log('Selected date 20');
    }
});

When("User edit used Technology", async () => {
    const xpath = '//*[@id="form-edit"]/div[6]/div[1]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    } else {
        console.log('Element not found')
    }
    for (const option of newSelectTech) {
        if (option === 'React Native') {
            await page.getByLabel('React Native').check();
        } else if (option === 'PHP') {
            await page.getByLabel('PHP').check();
        } else if (option === 'Golang') {
            await page.getByLabel('Golang').check();
        } else if (option === 'HTML5') {
            await page.getByLabel('HTML5').check();
        } else if (option === 'Vue JS') {
            await page.getByLabel('Vue JS').check();
        } else if (option === 'Python') {
            await page.getByLabel('Python').check();
        } else if (option === 'JavaScript') {
            await page.getByLabel('JavaScript').check();
        } else if (option === 'NextJS') {
            await page.getByLabel('NextJS').check();
        }
    }
    await page.getByText('Upload New Picture Delete JPG, JPEG, GIF or PNG. Max size of 2MB Project Name').click();
});

When("User edit Guest Assigned", async () => {
    const xpath = '//*[@id="form-edit"]/div[7]/div[1]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    } else {
        console.log('Element not found')
    }
    const guest = 'Priliana Guest'
    await page.getByLabel(guest).check();
    await page.getByText('Upload New Picture Delete JPG, JPEG, GIF or PNG. Max size of 2MB Project Name').click();
});

When("User edit QA Assigned", async () => {
    const xpath = '//*[@id="form-edit"]/div[4]/div[2]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    } else {
        console.log('Element not found')
    }
    const QA = 'Priliana QA'
    await page.getByLabel(QA, { exact: true }).check();
    await page.getByText('Upload New Picture Delete JPG, JPEG, GIF or PNG. Max size of 2MB Project Name').click();
});

When("User edit used Platform Type", async () => {
    const xpath = '//*[@id="form-edit"]/div[5]/div[2]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    } else {
        console.log('Element not found')
    }
    const platform = 'Mobile Android'
    await page.getByLabel(platform, { exact: true }).check();
    await page.getByText('Upload New Picture Delete JPG, JPEG, GIF or PNG. Max size of 2MB Project Name').click();
});

When("User edit used Testing Application", async () => {
    const xpath = '//*[@id="form-edit"]/div[6]/div[2]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        element.click();
    } else {
        console.log('Element not found')
    }
    for (const option of newSelectTestApp) {
        if (option === 'Load Runner') {
            await page.getByLabel('Load Runner').check();
        } else if (option === 'Trello') {
            await page.getByLabel('Trello').check();
        } else if (option === 'Testcase Lab') {
            await page.getByLabel('Testcase Lab').check();
        } else if (option === 'Redmine') {
            await page.getByLabel('Redmine').check();
        } else if (option === 'TestRail') {
            await page.getByLabel('TestRail').check();
        } else if (option === 'Gherkin') {
            await page.getByLabel('Gherkin').check();
        } else if (option === 'K6') {
            await page.getByLabel('K6').check();
        } else if (option === 'Katalon') {
            await page.getByLabel('Katalon').check();
        } else if (option === 'Taiga') {
            await page.getByLabel('Taiga').check();
        } else if (option === 'Jira') {
            await page.getByLabel('Jira').check();
        }
    }
    await page.getByText('Upload New Picture Delete JPG, JPEG, GIF or PNG. Max size of 2MB Project Name').click();
});


Then("Edit Project success and data shown in data table", async () => {
    const xpath = '//*[@id="projectCardList"]/div[1]';
    await page.waitForSelector(`xpath=${xpath}`);

    // Click the element using the XPath locator
    const element = page.locator(xpath);
    const nameLocator = page.getByText(editProjectName).first();
    if (element) {
        await nameLocator.click();
        console.log(`App ${editProjectName} is successfuly created. \n`);
    } else {
        console.log(`App ${editProjectName} is failed to create. \n`);
    }
});

const editProj = editProjectName;
export { editProj }