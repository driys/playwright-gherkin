// addProjectStepDefinitions.ts
import { When, Then } from "@cucumber/cucumber";
import { page, timeout } from "../../../support/hooks";
import { projectName, projectDesc, selectTech, selectTestApp, projectType } from "../../../support/variables";
import path from "path";
import { expect } from "playwright/test";

timeout;


When(`User click the "Add New Project" button on Project page`, async () => {
    await page.getByRole('link', { name: ' Add New Project' }).click();
});

When("User input Project Logo", async () => {
    // await page.getByText('Choose File').click(); 
    const fileChooserPromise = page.waitForEvent('filechooser');
    await page.getByText('Choose File').click();
    const fileChooser = await fileChooserPromise;
    await fileChooser.setFiles(path.join(__dirname, '..\\..\\..\\support\\media\\Cat Black.jpg'));
    // await expect(fileChooser).toBeDefined(); // ini ga guna cuma biar gambarnya muncul
});

When("User input Project Name", async () => {
    await page.locator('input[name="name"]').click();
    await page.locator('input[name="name"]').fill(projectName);
});

When("User input Project Description", async () => {
    await page.locator('textarea[name="description"]').click();
    await page.locator('textarea[name="description"]').fill(projectDesc);
});

When("User select Project Type", async () => {
    await page.getByRole('button', { name: 'Please Select ' }).click();
    await page.locator('#form-add').getByRole('list').locator('a').filter({ hasText: projectType }).click();
    console.log('\nSelected Project Type is ' + projectType + '\n')
    return projectType;
});

When("User select Project Start Date", async () => {
    await page.getByText('Start Date').click();
    await page.locator('input[name="start_date"]').click();

    // Try to select the 31st
    const date31 = page.getByRole('cell', { name: '31' }).first();
    if (await date31.isVisible()) {
        await date31.click();
        console.log('Selected date 31');
    } else {
        // If 31st is not visible, select the 28th
        const date28 = page.getByRole('cell', { name: '28' });
        await date28.waitFor({ state: 'visible' });
        await date28.click();
        console.log('Selected date 28');
    }
});

When("User select Project End Date", async () => {
    await page.getByText('End Date').click();
    await page.locator('input[name="end_date"]').click();
    await page.getByRole('cell', { name: '»' }).click();
    await page.getByRole('cell', { name: '»' }).click();
    await page.getByRole('cell', { name: '»' }).click();

    // Try to select the 31st
    const date31 = page.getByRole('cell', { name: '31' }).first();
    if (await date31.isVisible()) {
        await date31.click();
        console.log('Selected date 31');
    } else {
        // If 31st is not visible, select the 28th
        const date28 = page.getByRole('cell', { name: '28' });
        await date28.waitFor({ state: 'visible' });
        await date28.click();
        console.log('Selected date 28');
    }
});

When("User select used Technology", async () => {
    await page.getByRole('button', { name: '', exact: true }).nth(2).click();
    // Define a set of available technologies on the page
    const availableTech = new Set(['React Native', 'PHP', 'Golang', 'HTML5', 'Vue JS', 'Python', 'JavaScript', 'NextJS']);
    // Loop through selectTech and check only available technologies
    for (const option of selectTech) {
        if (availableTech.has(option)) {
            await page.getByLabel(option).check();
        }
    }
    await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
});

When("User select Guest Assigned", async () => {
    await page.getByRole('button', { name: '', exact: true }).nth(3).click();
    await page.getByLabel('Haris Abdullah Guest', { exact: true }).check();
    await page.getByLabel('Anggita Guest', { exact: true }).check();
    await page.getByLabel('Tharissa Guest', { exact: true }).check();
    await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
});

When("User select QA Assigned", async () => {
    await page.getByRole('button', { name: '', exact: true }).first().click();
    await page.getByLabel('Haris Abdullah QA', { exact: true }).check();
    await page.getByLabel('Haris QA', { exact: true }).check();
    await page.getByLabel('Haris Dummy', { exact: true }).check();
    await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
});

When("User select used Platform Type", async () => {
    await page.locator('#form-add div').filter({ hasText: 'Start Date End Date Desktop' }).getByRole('button').click();
    await page.getByLabel('Website', { exact: true }).check();
    await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
});

When("User select used Testing Application", async () => {
    await page.getByRole('button', { name: '', exact: true }).click();
    // Define a set of available testinga apps on the page
    const availableTestApp = new Set(['Load Runner', 'Trello', 'Testcase Lab', 'Redmine', 'TestRail', 'Gherkin', 'K6', 'Katalon', 'Taiga', 'Jira']);
    for (const option of selectTestApp) {
        if (availableTestApp.has(option)) {
            await page.getByLabel(option).check();
        }
    }
    await page.getByText('Choose File JPG, JPEG, GIF or PNG. Max size of 2MB Project Name Project').click();
});

Then("Add New Project success and data shown in data table", async () => {
    const xpath = '//*[@id="projectCardList"]/div[1]';
    await page.waitForSelector(`xpath=${xpath}`);

    // Click the element using the XPath locator
    const element = page.locator(xpath);
    const nameLocator = page.getByText(projectName).first();
    if (element) {
        await nameLocator.click();
        console.log(`${projectName} is successfuly created. \n`);
    } else {
        console.log(`${projectName} is failed to create. \n`);
    }
});

const proj = projectName;
export { proj }