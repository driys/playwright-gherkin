// searchFeatureStepDefinitions.ts
import { When, Then, } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { editFeatureName, editFeatureDetail, resultStatus, featureName } from "../../../../support/variables";
import { expect } from "playwright/test";

// NOTES :
// - Data dengan Scenario sama pasti akan ngambil di file otherkeyword
// - fungsi hooks dipake untuk pengambilan awal
// - Bikin fungsi variable harus di push lalu di export

timeout;

When(`User click button "Edit Feature" on feature detail page`, async () => {
    const xpath = '//*[@id="feature-content"]/div[1]/div/a[1]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.click();
    } else {
        console.log('Element not found');
    }
});

When(`User click button "Edit Feature" on feature page`, async () => {
    await page.getByRole('row', { name: featureName }).getByRole('link').nth(1).click();
});

When("User edit Feature Name", async () => {
    const xpath = '//*[@id="form-edit"]/div[1]/div/input[2]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.fill(editFeatureName);
    } else {
        console.log('Element not found');
    }
});

When("User edit QA Assigned to", async () => {
    const xpath = '//*[@id="form-edit"]/div[3]/div[1]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.click();
    } else {
        console.log('Element not found');
    }
    await page.getByRole('checkbox', { name: 'Haris Abdullah' }).uncheck();
});

When('User edit Status Feature', async () => {
    const xpath = '//*[@id="sortDropdown"]';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.last().click();
        await page.getByRole('link', { name: resultStatus }).click();
    } else {
        console.log('Element not found');
    }
})

When('User edit Feature Detail', async () => {
    const xpath = '//*[@id="form-edit"]/div[4]/div/textarea';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
        await element.fill(editFeatureDetail);
    } else {
        console.log('Element not found');
    }
});

Then('Edit feature success and edited data shown in data table', async () => {
    const locator = page.getByRole('link', { name: editFeatureName });
    if (locator) {
        console.log(`${editFeatureName} is successfully edited \n`);
    } else {
        console.log(`Failed to edit ${featureName} \n`)
    }
});

Then('Edit feature success and feature name changes to new name', async () => {
    const locator = page.getByRole('heading', { name: editFeatureName });
    if (locator) {
        console.log(`${editFeatureName} is successfully edited \n`);
    } else {
        console.log(`Failed to edit ${featureName} \n`)
    }
});
