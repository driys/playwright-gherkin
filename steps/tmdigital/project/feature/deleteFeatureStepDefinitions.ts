import { When, Then, } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { expect } from "playwright/test";
import { editFeatureName, featureName } from "../../../../support/variables";

timeout;

When(`User click the "Delete Feature" button on feature page`, async () => {
  const xpath = '//*[@id="featureDataTable"]/tbody/tr[1]/td[6]/ul/li[2]/a/img';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Delete Feature" button on feature detail page`, async () => {
  const xpath = '//*[@id="btnDeleteFeature"]';
  await page.waitForSelector(`xpath=${xpath}`);

  // Click the element using the XPath locator
  const element = page.locator(xpath);
  if (element) {
    await element.click();
  } else {
    console.log('Element not found');
  }
});

When(`User click the "Yes, Delete" button on feature modal`, async () => {
  await page.getByRole('button', { name: 'Yes, Delete' }).click();
});

Then('Feature should be deleted from data table', async () => {
  const locatorAdd = await page.getByRole('row', { name: featureName }).isHidden();
  const locatorEdit = await page.getByRole('row', { name: editFeatureName }).isHidden();
  
  if (locatorAdd && locatorEdit) {
    console.log(`Feature is successfully deleted \n`);
  } else {
    console.log(`Failed to delete feature \n`);
  }  
});