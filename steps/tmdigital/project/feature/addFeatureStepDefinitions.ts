// addsFeatureStepDefinitions.ts
import { When, Then,} from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { featureDetail, featureName} from "../../../../support/variables";
import { expect } from "playwright/test";

// NOTES :
// - Data dengan Scenario sama pasti akan ngambil di file otherkeyword
// - fungsi hooks dipake untuk pengambilan awal
// - Bikin fungsi variable harus di push lalu di export

timeout;

When(`User click button "Add New Feature"`, async () => {
    const xpath = '//*[@id="content"]/div[1]/div[3]/div[2]/div[1]/div/div[1]/div/button';
    await page.waitForSelector(`xpath=${xpath}`);
    const element = page.locator(xpath);
    if (element) {
      await element.click();
    } else {
      console.log('Element not found');
    }  
    // await page.getByRole('button', { name: ' Add New Features' }).click();
});
When("User input Feature Name", async () => {
    await page.getByRole('dialog', { name: 'Delete Project' }).locator('input[name="name"]').click();
    await page.getByRole('dialog', { name: 'Delete Project' }).locator('input[name="name"]').fill(featureName);
});
When("User select QA Assigned to", async () => {
    await page.getByRole('button', { name: '', exact: true }).click();
    await page.getByRole('checkbox', { name: 'Select all' }).check();
});
When('User input Feature Detail', async () => {
    await page.getByRole('dialog', { name: 'Delete Project' }).locator('textarea[name="description"]').click();
    await page.getByRole('dialog', { name: 'Delete Project' }).locator('textarea[name="description"]').fill(featureDetail);
});

Then('Add New Feature success and data shown in data table', async () => {
    const locator = page.getByRole('link', { name: featureName });
    await expect(locator).toBeVisible();
    if (locator) {
        console.log(`${featureName} is successfully created \n`);
    } else {
        console.log(`${featureName} is failed to create \n`)
    }
});