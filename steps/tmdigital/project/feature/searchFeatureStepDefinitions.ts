import { Given, Then, When } from "@cucumber/cucumber";
import { page, timeout } from "../../../../support/hooks";
import { editFeatureName, featureName } from "../../../../support/variables";
import { expect } from "playwright/test";

timeout; //Untuk waiting element muncul yang udah di set di hooks


When("User input field search feature", async () => {
    await page.getByPlaceholder('Search by Feature Name').click();
    // await page.getByPlaceholder('Search by Feature Name').fill(featureName); // Uncomment to fill the field with new feature name
    await page.getByPlaceholder('Search by Feature Name').fill(editFeatureName); // Uncomment to fill the field with edited feature name
})

Then("Show list data feature who was search before", async () => {
    const locatorAdd = page.getByRole('link', { name: featureName });
    const locatorEdit = page.getByRole('link', { name: editFeatureName });
    if (await locatorAdd.isVisible()) {
        await expect(locatorAdd).toBeVisible();
        console.log(`${featureName} is found \n`);
    } else if (await locatorEdit.isVisible()) {
        await expect(locatorEdit).toBeVisible();
        console.log(`${editFeatureName} is found \n`);
    } else {
        console.log(`feature is not found \n`)
    }
})
