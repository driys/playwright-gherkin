// loginSteps.ts
import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Browser, Page } from "playwright/test";

let browser: Browser;
let page: Page;

setDefaultTimeout(20 * 1000);

Given("I am on the OpenMRS login page", async () => {
  browser = await chromium.launch();
  const context = await browser.newContext();
  page = await context.newPage();
  await page.goto("https://demo.openmrs.org/openmrs/login.htm");
});

When(
  "I enter a valid username {string} and password {string}",
  async (username: string, password: string) => {
    await page.fill('input[name="username"]', username);
    await page.fill('input[name="password"]', password);
  }
);

When("I select Location", async () => {
  await page.getByText("Inpatient Ward").click();
});

When('I click the "Login" button', async () => {
  await page.click('input[type="submit"]');
});

Then("I should be logged in successfully", async () => {
  // Add assertions or verifications here
  // For example, you can assert that a certain element is present on the dashboard page
  await page.getByRole('navigation').getByRole('link').first().isVisible();
  await browser.close();
});
