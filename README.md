# Playwright Gherkin

This repository is a shared or public repository, please use it responsibly.

## Prerequisites

- Node.js (version 12 or higher)
- npm (version 6 or higher)

### Installing Node.js and npm

To install Node.js and npm, follow these steps:

1. **Download Node.js**:
    - Visit the [Node.js download page](https://nodejs.org/).
    - Download the LTS (Long Term Support) version for your operating system.

2. **Install Node.js**:
    - Run the downloaded installer and follow the installation instructions.
    - The installer will also install npm automatically.

3. **Verify Installation**:
    - Open a terminal or command prompt.
    - Verify that Node.js is installed by running:
      ```bash
      node -v
      ```
      This should display the version of Node.js installed.
    - Verify that npm is installed by running:
      ```bash
      npm -v
      ```
      This should display the version of npm installed.

## Installation

1. **Clone the repository** (if you haven't already):

    ```bash
    git clone https://gitlab.com/driys/playwright-gherkin.git
    cd your-repository
    ```

2. **Install Playwright**:

    ```bash
    npm install playwright
    ```

3. **Install Cucumber.js**:

    ```bash
    npm install @cucumber/cucumber
    ```

4. **Install TypeScript** as a development dependency:

    ```bash
    npm install typescript --save-dev
    ```

5. **Install Playwright Test Runner** as a development dependency:

    ```bash
    npm install @playwright/test --save-dev
    ```

6. **Install Playwright Test Runner** as a development dependency:

    ```bash
    npm install --save-dev @playwright/test ts-node
    ```

7. **Cucumber Reporter Playwright**:

    ```bash
    npm install cucumber-html-reporter --save-dev
    ```

8. **Initialize Playwright**:

    ```bash
    npx playwright install
    ```

## Writing Script

1. **Write your feature file under the feature folder**

    ```
    playwright-gherkin/
    ├── features/
    │ ├── module_folder/
      │  └── example.feature
    ```

2. **Write and define your step definition in the step definition folder according to the feature file you have written earlier**

    ```
    playwright-gherkin/
    ├── steps/
    │ ├── module_folder/
      │  └── example.ts
    ```

3. **Write or modify the support file as needed, do not modify the support file too much if it not important or not needed.**

    ```
    playwright-gherkin/
    ├── support/
    │ └── example_support.ts
    ```

## Running Script

**Run your script using node runner and run the main.js file in the support folder**

```
node support/main.js
```

## Contact

**If you have any question or suggest about this repository, you can reach me at _haris.abdullah96@gmail.com_**

## License

[MIT](https://choosealicense.com/licenses/mit/)